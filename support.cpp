#include "support.h"
#include <stdlib.h>
#include <string.h>
#include "common.h"

CommandGeneral * CommandGeneral::create( const char * bf, const int sz)
{
	CommandGeneral * rv;
	rv = Command50::canCreate( bf, sz);
	if ( rv != NULL) return rv;
	rv = Command60::canCreate( bf, sz);
	return rv;
}

Command50 * Command50::canCreate(const char * bf, const int sz)
{
	// counting number of occurencies of ']'
	char * start_pos = const_cast<char *>(reinterpret_cast<const char*>(memchr( bf, '[', sz  )));
	if( start_pos == NULL) return NULL;
	start_pos++;

	char * CMD_POS, *STV_POS, *ENV_POS, * DLY_POS, *TERM_POS;
	CMD_POS= const_cast<char *>(reinterpret_cast<const char*>( memchr( start_pos, '[', sz + ( bf - start_pos ) )));
	if( CMD_POS == NULL) return NULL;
	CMD_POS++;
	int CMD_VAL = atoi( CMD_POS);
	if( CMD_VAL != atoi ( CMD_ONETYPE_CODE ) ) return NULL; 
	STV_POS= const_cast<char *>(reinterpret_cast<const char*>(memchr( CMD_POS, '[', sz + (bf - CMD_POS) )));
	if( STV_POS == NULL) return NULL;
	STV_POS++;
	ENV_POS = const_cast<char *>(reinterpret_cast<const char*>(memchr( STV_POS, '[', sz + (bf - STV_POS )  )));
	if( ENV_POS == NULL) return NULL;
	ENV_POS++;
	DLY_POS = const_cast<char *>(reinterpret_cast<const char*>(memchr(ENV_POS, '[', sz + (bf - ENV_POS) )));
	if( DLY_POS == NULL) return NULL;
	DLY_POS++;
	TERM_POS =const_cast<char *>(reinterpret_cast<const char*>( memchr( DLY_POS, ']', sz+ (bf - DLY_POS) )));
	if( TERM_POS == NULL) return NULL;
	return new Command50( atoi( start_pos ), TERM_POS + 1 - bf,
			atoi ( STV_POS), atoi( ENV_POS ), atoi( DLY_POS));
}

Command60 * Command60::canCreate(const char * bf, const int sz)
{
	// counting number of occurencies of ']'
	char * start_pos = const_cast<char *>(reinterpret_cast<const char*>(memchr( bf, '[', sz  )));
	if( start_pos == NULL) return NULL;
	start_pos++;

	char * CMD_POS, *VL_POS, *EL_POS, * DLY_POS, *TERM_POS;
	CMD_POS= const_cast<char *>(reinterpret_cast<const char*>( memchr( start_pos, '[', sz + ( bf - start_pos ) )));
	if( CMD_POS == NULL) return NULL;
	CMD_POS++;
	int CMD_VAL = atoi( CMD_POS);
	if( CMD_VAL != atoi ( CMD_ONETYPE_CODE ) ) return NULL; 
	VL_POS= const_cast<char *>(reinterpret_cast<const char*>(memchr( CMD_POS, '[', sz + (bf - CMD_POS) )));
	if( VL_POS == NULL) return NULL;
	VL_POS++;
	int arraySize = atoi( VL_POS );
	if( arraySize < 1) return NULL;
	std::vector<int> Preparation( arraySize);
	EL_POS = VL_POS;
	for( int i = 0; i < arraySize; ++i)
	{
		EL_POS = const_cast<char *>(reinterpret_cast<const char*>(memchr( EL_POS, '[', sz + (bf - EL_POS ))));
		if( EL_POS == NULL) return NULL;
		EL_POS++;
		Preparation[i] = atoi( EL_POS);
	}
	DLY_POS = const_cast<char *>(reinterpret_cast<const char*>(memchr(EL_POS, '[', sz + (bf - EL_POS) )));
	if( DLY_POS == NULL) return NULL;
	DLY_POS++;
	TERM_POS =const_cast<char *>(reinterpret_cast<const char*>( memchr( DLY_POS, ']', sz+ (bf - DLY_POS) )));
	if( TERM_POS == NULL) return NULL;
	return new Command60( atoi( start_pos ), TERM_POS + 1 - bf,
			Preparation, atoi( DLY_POS));
}

void CommandGeneral::markBytesAsSent( int num)
{
	if ( num < total2Send && num > 0 ) {
		total2Send -= num;
		memmove( ACK_BUFF, ACK_BUFF + num, total2Send );
	} else {
		total2Send = 0;
	}
}

bool CommandGeneral::bothACKsSent() const
{
	// TODO
	return true;
}
