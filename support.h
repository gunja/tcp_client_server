#ifndef SUPPORT_H
#define SUPPORT_H

#include "server.h"
#include <vector>
#include <list>
#include <stdio.h>
#include <string.h>


class CommandGeneral
{
protected:
	int id;
	int CMD;
	int consum;
	int DelayValue;
	char ACK_BUFF[INITIAL_BF_SZ];
	int total2Send;
	CommandGeneral(const int &_id,const int &cmd,
		const int &con,const int &dv):
		id(_id), CMD( cmd), consum( con), DelayValue(dv) {
			sprintf( ACK_BUFF, "[%d][50][1]", id );
			total2Send = strlen( ACK_BUFF);
		};
public:
	static CommandGeneral * create(const char * bf, const int);
	int consumed()const { return consum;};
	bool bothACKsSent() const;
	bool hasOutData() const{ return total2Send != 0;};
	void markBytesAsSent( int num);
	const char * getDataPtr() const { return ACK_BUFF;};
	int sentBytesRemains() const { return total2Send; };
};

class Command50: public CommandGeneral {
	int Start, End;
protected:
	Command50(const int &_id,
		const  int &con, const int & _Start, int _End, int DelVal):
		CommandGeneral(_id, 50, con, DelVal), Start(_Start),
			End( _End) {
		};
public:
	static Command50 * canCreate(const char * bf, const int);
};

class Command60: public CommandGeneral {
	std::vector<int> ValuesArray;
protected:
	Command60(const int &_id,
		const  int &con, const std::vector<int> & vecIn, int DelVal):
		CommandGeneral(_id, 60, con, DelVal), ValuesArray(vecIn) {};
public:
	static Command60 * canCreate(const char * bf, const int);
};

class CommandList {
public:
	std::list<CommandGeneral* > lst;
	~CommandList() {
		for( std::list<CommandGeneral*>::iterator i= lst.begin();
			i != lst.end(); ++i)
			delete *i;
	};
};

#endif
