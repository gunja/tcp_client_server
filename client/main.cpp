/**
\file client.cpp
Sources for Windows-side of communications. Qt based
*/
#include <stdio.h>
#include <errno.h>
#include <stdint.h>
#include <stdlib.h>
#include "client.h"
// #include <QCommandLineParser> // only Qt 5.X supported

void show_help(const char *nm);

int main(int argc, char * argv[])
{
	/* TODO parce command line arguments
		consider application loop
	*/
	QString address("localhost");
	int port = SERVER_PORT_NUM;
	if( argc > 1)
		address = QString( argv[1]);

	TCPClientClass client( address, port);
	if( ! client.makeConnection())
	{
		exit(-1);
	}
	int id_Seq = 1;
	client.TCP_Client_send( id_Seq++, 1, 2, 100 );
	std::vector<int> vec;
	for( int i =0; i < 24; ++i) vec.push_back( (i * 17 ) %37 );
	client.TCP_Client_send( id_Seq++, vec, 100 );
	while( ! client.allConfirmed() )
	{
		client.makestep();
	}
	return 0;
}

void show_help(const char *nm)
{
	const char * ptr = nm;
	const char defName[] = "server";
	if ( NULL == nm )
		ptr = defName;
	printf("Calling conversion: %s {-h,[-p port] [-a address]}\n", ptr );
	printf("\twhere:\t -h will show this help and exit\n" );
	printf("\t\t -p port will set \"port\" number to be "
		"opened instead default one (%d)\n", SERVER_PORT_NUM );
	printf("\t\t -a address will set \"hostname\" to be "
		"conncted instead default one (localhost)\n");
}

