#ifndef CLIENT_H
#define CLIENT_H

#include <QString>
#include "common.h"
#include <vector>
#include <QByteArray>
#include <QList>
#include <QTcpSocket>
#include <QObject>
#include "task.h"

typedef std::vector<int> IntVector;
typedef QList<TaskClass> TasksList;

class TCPClientClass: public QObject {
	Q_OBJECT
	QTcpSocket tcpSocket;
	qint64 alreadyRead;
	int port;
	QString addressName;
	TasksList tasks;
	bool appendToTaskQueue(const int id, const QByteArray &);
	void sendDataFromList();
	void checkIncomingData();
	void sparseTaskQueue();
	char * ReadingBuff;
public:
	TCPClientClass( QString nm, int _port= SERVER_PORT_NUM);
	~TCPClientClass();
	bool TCP_Client_send(const unsigned int id,
		IntVector index,
		const unsigned int delay_ms);
	bool TCP_Client_send(const unsigned int id,
		const unsigned int strt,
		const unsigned int end,
		const unsigned int delay_ms);
	bool allConfirmed() const;
	void makestep();
	bool makeConnection();
};

// known TODO:
//		1 create "task" class -- about to be completed
//		2 add queue of tasks -- QList solves the problem
//		etc --
// TODO establish connection

#endif
