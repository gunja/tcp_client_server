#include "client.h"
#include <QThread>
#ifdef __WIN32
#include <WinBase.h>
#else
#include <unistd.h>
#endif

TCPClientClass::TCPClientClass( QString nm, int _prt):
	tcpSocket( this), alreadyRead(0), port( _prt), addressName( nm)  {
	ReadingBuff = new char[2000];
}

TCPClientClass::~TCPClientClass() {
	// TODO
	// close socket if it's opened
	// delete each "task" object
	// something else?
	delete[] ReadingBuff;
	tcpSocket.close();
}


bool TCPClientClass::TCP_Client_send(const unsigned int id,
		IntVector index,
		const unsigned int delay_ms)
{
	int arSize = index.size();
	QString sendMessage = QString("[%1][60][%2]").arg( id).arg( arSize);
	for( int i=0; i < arSize; ++i) {
		sendMessage += QString("[%1]").arg( index[i]);
	}
	sendMessage += QString("[%1]").arg( delay_ms );
	appendToTaskQueue(id, sendMessage.toLocal8Bit());
	sendDataFromList();
	checkIncomingData();
}

bool TCPClientClass::TCP_Client_send(const unsigned int id,
		const unsigned int strt,
		const unsigned int end,
		const unsigned int delay_ms)
{
	QString sendMessage = QString("[%1][50][%2][%3][%4]").arg( id).arg(strt).
		arg(end).arg(delay_ms);
	appendToTaskQueue(id , sendMessage.toLocal8Bit());
	sendDataFromList();
}

bool TCPClientClass::appendToTaskQueue(const int id, const QByteArray & arr)
{
	tasks.append( TaskClass( id, arr) );
}

bool TCPClientClass::allConfirmed() const
{
	// FIXME implement this function
	bool rv = true;
	for( TasksList::const_iterator i = tasks.begin(); i != tasks.end(); ++i )
	{
		if( ! (*i).HasBothConfirms() )
			rv = false;
	}
	return rv;
}

void TCPClientClass::makestep()
{
	// FIXME implement this function
	sendDataFromList();
	checkIncomingData();
	return;
}

void TCPClientClass::sendDataFromList()
{
	for( TasksList::iterator i = tasks.begin(); i != tasks.end(); ++i )
	{
		qint64 rv;
		if( i->DataForOutputCount() > 0 ) {
			rv = tcpSocket.write( i->getConstRemainData(),
				i->DataForOutputCount() );
			if( rv > 0 )
				i->encreaseSent( rv);
			break;
		}
	}
}

void TCPClientClass::checkIncomingData()
{
	if( tcpSocket.bytesAvailable() > 0) {
		qint64 r = tcpSocket.read( ReadingBuff + alreadyRead, 2000 - alreadyRead );
		// if I can convert received data to confirmation packet - reduce
		// queue and continue parcing
		// TODO
	}
}

void TCPClientClass::sparseTaskQueue()
{
	for(int i= tasks.size(); i >=0 ; --i )
	{
		if( tasks[i].HasBothConfirms() )
			tasks.removeAt( i);
	}
}

bool TCPClientClass::makeConnection()
{
	tcpSocket.connectToHost( addressName, port);
	int cnt=0;
	while( cnt < 100 ||  tcpSocket.state() !=  QAbstractSocket::ConnectedState ) {
		//QThread::msleep(100);
		// TODO fall asleep LINUX specific
#ifdef __WIN32
		Sleep( 100);
#else
		sleep(100);
#endif
		cnt ++;
	}
	return QAbstractSocket::ConnectedState == tcpSocket.state();
}

