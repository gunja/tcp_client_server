#ifndef TASK_DESC_H
#define TASK_DESC_H

#include <QObject>
#include <QByteArray>

class TaskClass  {
	QByteArray DataOut;
	int Id;
	qint64 sendRem;
	qint64 alreadySent;
	bool ACK1, ACK2;
public:
	TaskClass(const int _id, const QByteArray & snd);
	bool HasBothConfirms() const { return ACK1 && ACK2 ; };
	bool HasFirstConfirm() const { return ACK1;};
	int DataForOutputCount() const { return sendRem ; };
	int getId() const { return Id; };
	const char * getConstRemainData() const { return DataOut.constData() + alreadySent; };
	void encreaseSent(qint64 rv) { alreadySent+= rv; sendRem-= rv;};
	void setFirstACK() { ACK1 = true;};
	void setSecondACK() { ACK2 = true;};
};

#endif
