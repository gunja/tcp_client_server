/**
	\file main.cpp
	entry point and constructor of server class
*/

#include "server.h"
#include <stdio.h>
#include <errno.h>
#include <stdint.h>
#include <stdlib.h>
#include "common.h"
#include <unistd.h>
#include <sys/types.h>
#include <string.h>


void show_help(const char *nm);

int main(int argc, char * argv[])
{
	int port, opt;

	port = SERVER_PORT_NUM; 
	while ((opt = getopt(argc, argv, "hp:")) != -1) {
	    switch (opt) {
		case 'h':
			show_help( argv[0]);
			exit(EXIT_SUCCESS);
		case 'p':
			port = atoi(optarg);
			if( port < 0) {
				show_help(argv[0]);
				exit(EXIT_FAILURE);
			}
			break;
		default: /* '?' */
			show_help(argv[0]);
			exit(EXIT_FAILURE);
	    }
	}
	TCPLinuxServer server( port);
	server.execute();
	return 0;
}

void show_help(const char *nm)
{
	const char * ptr = nm;
	const char defName[] = "server";
	if ( NULL == nm )
		ptr = defName;
	printf("Calling conversion: %s {-h,[-p port]}\n", ptr );
	printf("\twhere:\t -h will show this help and exit\n" );
	printf("\t\t -p port will set \"port\" number to be "
		"opened instead default one (%d)\n", SERVER_PORT_NUM );
}

