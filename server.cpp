/**
\file server.c
Sources for linux-side of communications
*/

#include "server.h"
#include <string.h>
#include <pthread.h>
#include <unistd.h>
#include <fcntl.h>
#include <poll.h>
#include "support.h"
#include <list>

int TCPLinuxServer::execute()
{
	int n_sock;
	listenSocket = socket(SOCK_STREAM,
			AF_INET, 0);
	if( listenSocket < 0) return -1;
	memset(&my_addr, 0, sizeof(struct sockaddr_in));
	my_addr.sin_family = AF_INET;
	my_addr.sin_port = htons( pReq );
	/* my_addr.sin_addr is nulled, so address is 0.0.0.0 */

	if (bind(listenSocket,
		reinterpret_cast<struct sockaddr *> (&my_addr),
                   sizeof(struct sockaddr_in)) == -1)
	{
		return -2;
	}

	if (! listen(listenSocket, LISTEN_BACKLOG) )
		return -3;
	while( keepOn ) {
		socklen_t ln = sizeof( peer_addr);
		n_sock = accept( listenSocket,
			reinterpret_cast<sockaddr*>(&peer_addr),
			&ln );
		if ( n_sock < 0)
			return -4;
		int * sock_ptr = new int;
		*sock_ptr = n_sock;
		pthread_t tid;
		pthread_attr_t attr;
		pthread_attr_init(&attr);
		pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
		pthread_create( &tid, &attr,
			TCPLinuxServer::func, sock_ptr );
	} 
	return 0;
}

TCPLinuxServer::~TCPLinuxServer()
{
	if( listenSocket >=0 )
		close( listenSocket );
}


void * TCPLinuxServer::func( void * arg)
{
	int *v = reinterpret_cast<int *>(arg);
	int sock = *v;
	delete v;
	char * buf = new char[INITIAL_BF_SZ];
	int flags = fcntl(sock, F_GETFL, 0);
   	flags |= O_NONBLOCK;
	fcntl(sock, F_SETFL, flags);
	struct pollfd pfd;
	pfd.fd = sock;         /* file descriptor */
        pfd.events = POLLIN | POLLOUT ;     /* requested events */
	int TimeOutsCounter = 0;
	int curRead =0;
	CommandList cmdLst;
	while( 1 ) {
		/* connection attempts to last forever*/
		int rv = poll(&pfd, 1, POLL_TO_MS);
		if( rv == 0) {
			// there were nothing new on socket.
			// lets' start counting
			TimeOutsCounter++;
		} else {
			TimeOutsCounter = 0;
		}
		if( TimeOutsCounter/ 20 > MAX_KEEP_SEC) {
			delete buf;
			return NULL;
		}
		if( rv < 0) {
			// error situation. undefined how to handle it
			delete buf;
			return NULL;
		}
		if( pfd.revents & (POLLIN | POLLPRI ) )
		{
			int r = read( sock, buf + curRead, INITIAL_BF_SZ - curRead );
			// TODO make proper conversion
			CommandGeneral * ptr = CommandGeneral::create( buf, curRead + r);
			if ( NULL != ptr )
			{
				cmdLst.lst.push_back( ptr);
				if( ptr->consumed() < curRead + r ) {
					memmove( buf, buf + ptr->consumed(),
						curRead + r - ptr->consumed() );
					curRead = curRead + r - ptr->consumed();
				}
				else
					curRead = 0;
			}
		}
		if( pfd.revents & POLLOUT )
		{
			// TODO if there are data left to write - write it
			// int r = write( sock, wrBuf, size);
			for( std::list<CommandGeneral*>::iterator i= cmdLst.lst.begin();
				i != cmdLst.lst.end(); ++i) {
				if( (*i)->hasOutData() ) {
					int r = write( sock, (*i)->getDataPtr(),
						(*i)->sentBytesRemains() );
					if( r > 0)
						(*i)->markBytesAsSent(r);
					break;
				}
			}
			
		}
		if( pfd.revents & ( POLLERR | POLLHUP | POLLNVAL ) ) {
			delete buf;
			return NULL;
		}
		for( std::list<CommandGeneral*>::iterator i= cmdLst.lst.begin();
				i != cmdLst.lst.end(); ++i) {
			if( (*i)->bothACKsSent() ) {
				delete *i;
				cmdLst.lst.erase( i);
			}
		}

	}
	return NULL;
}

