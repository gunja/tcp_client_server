/**
	\file server.h
	Declarations of  server class
*/
#include "common.h"

#ifndef SERVER_C_H
#define SERVER_C_H

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define INITIAL_BF_SZ	(2000)
#define POLL_TO_MS	(20000)
#define MAX_KEEP_SEC	(100)

class TCPLinuxServer {
	int listenSocket, pReq, keepOn;
	struct sockaddr_in my_addr, peer_addr;
	socklen_t peer_addr_size;
public:
	TCPLinuxServer( int port = SERVER_PORT_NUM ): pReq( SERVER_PORT_NUM), keepOn(1) {} ;
	~TCPLinuxServer();
	int execute();
	static void * func(void *);
};

#endif

