/**
	\file common.h
	File contains common for server and client values and defines
*/

#ifndef COMMON_H
#define COMMON_H

#define SERVER_PORT_NUM	54327
#define LISTEN_BACKLOG 10

#define CMD_ONETYPE_CODE "50"
#define CMD_TWOTYPE_CODE "60"

#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif


#endif
