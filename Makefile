OBJS=server.o main.o support.o

all: server

server: $(OBJS)
	g++ $(OBJS) -o $@ -lpthread

$(OBJS): %.o:	%.cpp
	g++ -c $< -o $@ 

clean:
	rm -rf *.o
	rm -rf server
